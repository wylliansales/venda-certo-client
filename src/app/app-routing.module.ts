import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent }   from './main/main.component';
import { DashboardComponent }  from './dashboard/dashboard.component';
import { BlankComponent } from './blank/blank.component';
import { LoginComponent }  from './session/login/login.component';
import { RegisterComponent }  from './session/register/register.component';
import { ForgotPasswordComponent }  from './session/forgot-password/forgot-password.component';
import { LockScreenComponent }  from './session/lockscreen/lockscreen.component';

const appRoutes: Routes = [
	{	
		path: 'session/login',
		component: LoginComponent,
	},{	
		path: 'session/register',
		component: RegisterComponent,
	},{	
		path: 'session/forgot-password',
		component: ForgotPasswordComponent,
	},{	
		path: 'session/lockscreen',
		component: LockScreenComponent,
	},{
	 	path: '',
	 	component: MainComponent,
	 	children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'pages/blank', component: BlankComponent},
			{ path: '', component: DashboardComponent }
	 	]
  	}
];

@NgModule({
  	imports: [RouterModule.forRoot(appRoutes)],
 	exports: [RouterModule],
  	providers: []
})
export class RoutingModule { }
